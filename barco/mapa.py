from graphics import *
from math import sqrt
#José Fernandes, 100209
#Nuno Fernandes, 100242


class Cais:
    def __init__(self, mapa, cais):
        self.c = cais[0]
        self.c.setFill(color_rgb(116, 40, 2))
        self.c.setOutline(color_rgb(116, 40, 2))
        self.c.draw(mapa)


class Ilha:
    def __init__(self, mapa, ilhas):
        for k in ilhas:
            obstaculo = k
            obstaculo.setFill('green')
            obstaculo.setOutline('green')
            obstaculo.draw(mapa)

    def limite_obstaculos(self, ilhas, imp_ponto):
        n = 0
        while n < (len(ilhas)):
            if (((imp_ponto.getX() - ilhas[n].getCenter().getX()) ** 2 + (imp_ponto.getY() - ilhas[n].getCenter().getY()) ** 2) <= (ilhas[n].getRadius() + 2) ** 2) or (((imp_ponto.getX())** 2 + (imp_ponto.getY())**2 <= (sqrt(50)+1)  ** 2)):
                return 'false'
            else:
                n = n + 1
        else:
            return 'true'


class Impurezas:
    def __init__(self, mapa, impurezas):
        self.impurezas = []
        for k in impurezas:
            self.i = Circle(k, 0.5)
            self.i.setFill('black')
            self.i.setOutline('black')
            self.i.draw(mapa)
            self.impurezas.append(self.i)

    def apaga_impureza(self, k):
        self.impurezas[k].undraw()

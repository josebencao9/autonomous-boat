from mapa import *
from animation import *
from time import *
from dados import *
from graphics import *
from random import randrange
#José Fernandes, 100209
#Nuno Fernandes, 100242

def main3b():
    lista_cais = [Rectangle(Point(0, 0), Point(5, 5))]
    lista_janela = [800, 600]
    lista_impurezas = []
    lista_distancia_impureza_temporaria = []
    lista_impurezas_ordenada = []
    lista_ilhas = []
    lista_ilhas_possiveis = [Circle(Point(50, 50), 5), Circle(Point(20, 30), 3), Circle(Point(10, 10), 3),
                             Circle(Point(70, 45), 5), Circle(Point(75, 25), 4), Circle(Point(8, 40), 3),
                             Circle(Point(12, 50), 4), Circle(Point(50, 12), 3), Circle(Point(40, 30), 5),
                             Circle(Point(5, 20), 2)]
    for i in range(round(randrange(2, 5))):
        n = round(randrange(0, len(lista_ilhas_possiveis)))
        lista_ilhas.append(lista_ilhas_possiveis[n])
        del (lista_ilhas_possiveis[n])
    print(lista_ilhas)

    map = GraphWin('Mapa', lista_janela[0], lista_janela[1])
    map.setCoords(0, 0, lista_janela[0] / 10, lista_janela[1] / 10)
    update(600)
    map.setBackground(color_rgb(30, 143, 245))

    Cais(map, lista_cais)
    o = Ilha(map, lista_ilhas)

    while len(lista_impurezas) < 5:
        ponto_impureza = map.getMouse()
        if o.limite_obstaculos(lista_ilhas, ponto_impureza) == 'false':
            print('obs')
        else:
            ponto_temporario = Circle(ponto_impureza, 0.5)
            ponto_temporario.setWidth(1)
            ponto_temporario.draw(map)
            lista_impurezas.append(ponto_impureza)
            sleep(0.3)
            ponto_temporario.undraw()

    for i in lista_impurezas:
        distancia = sqrt((i.getX() - lista_cais[0].getCenter().getX()) ** 2 + (
                i.getY() - lista_cais[0].getCenter().getY()) ** 2)
        lista_distancia_impureza_temporaria.append(tuple([i, distancia]))
    print(lista_distancia_impureza_temporaria)
    lista_distancia_impureza_temporaria.sort(reverse=False, key=lambda x: x[1])
    print(lista_distancia_impureza_temporaria)

    for i in lista_distancia_impureza_temporaria:
        x = list(i)
        lista_impurezas_ordenada.append(x[0])
    print(lista_impurezas_ordenada)

    i = Impurezas(map, lista_impurezas_ordenada)
    largura = abs(lista_cais[0].getP1().getX() - lista_cais[0].getP2().getX())

    roboat = ShotTracker(map, lista_cais, lista_ilhas, lista_impurezas_ordenada)

    for k in range(len(lista_impurezas_ordenada)):
        while abs(roboat.getX() - lista_impurezas_ordenada[k].getX()) > 0.1 or abs(
                roboat.getY() - lista_impurezas_ordenada[k].getY()) > 0.1:
            roboat.update(k)
        else:
            sleep(2)
            i.apaga_impureza(k)
    while abs(roboat.getX() - (lista_cais[0].getCenter().getX() + (largura / 2) + 3)) > 0.1 or abs(
            roboat.getY() - lista_cais[0].getCenter().getY()) > 0.1:
        roboat.update_retorna_cais()

    roboat.update_proa_atracar()
    sleep(4)


main3b()

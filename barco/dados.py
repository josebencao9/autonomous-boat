from graphics import *
from math import*
#José Fernandes, 100209
#Nuno Fernandes, 100242

class Dados:
    def __init__(self, ambiente, impurezas):
    #importa dados de ficheiros e organiza-os em listas
        self.lista_cais = []
        self.lista_ilhas = []
        self.lista_janela = []
        self.lista_impurezas = []
        self.lista_distancia_impureza_temporaria=[]
        self.lista_impurezas_ordenada=[]
        ambiente = open(ambiente, 'r').readlines()
        self.distribuicao_listas(ambiente)
        impurezas = open(impurezas, 'r')
        self.ordenar_impurezas(impurezas)

    def ordenar_impurezas(self, impurezas):
    #ordena as impurezas com base na sua distancia ao cais
        for linha in impurezas:
            k = linha.split()
            self.lista_impurezas.append(Point(float(k[0]), float(k[1])))
        for i in self.lista_impurezas:
            distancia = sqrt((i.getX() - self.lista_cais[0].getCenter().getX()) ** 2 + (
                        i.getY() - self.lista_cais[0].getCenter().getY()) ** 2)
            self.lista_distancia_impureza_temporaria.append(tuple([i, distancia]))
        self.lista_distancia_impureza_temporaria.sort(reverse=False, key=lambda x: x[1])
        for i in self.lista_distancia_impureza_temporaria:
            x = list(i)
            self.lista_impurezas_ordenada.append(x[0])

    def distribuicao_listas(self, ambiente):
    #organiza os dados e adiona-os á lista correta de acordo com a primeira entrada de cada linha
        for linha in ambiente:
            k = linha.split()
            if k[0] == 'ILHA':
                self.lista_ilhas.append(eval(k[1]))
            elif k[0] == 'CAIS':
                self.lista_cais.append(eval(k[1]))
            elif k[0] == 'JANELA':
                self.lista_janela.append(float(k[1]))
                self.lista_janela.append(float(k[2]))

    def retorna_lista_cais(self):
    #retorna a lista que contém o objeto cais
        return self.lista_cais

    def retorna_lista_ilhas(self):
    #retorna a lista que contém os objetos ilhas
        return self.lista_ilhas

    def retorna_lista_janela(self):
    #retorna a lista que contém as dimensões da janela
        return self.lista_janela

    def retorna_lista_impurezas(self):
    #retorna a lista que contém os objetos impurezas
        return self.lista_impurezas_ordenada

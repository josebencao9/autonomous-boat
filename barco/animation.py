from math import sin, cos
from graphics import *
from projectile import *
#José Fernandes, 100209
#Nuno Fernandes, 100242

class ShotTracker:
    def __init__(self, win,cais,ilhas,impurezas):
    #representa graficamente o movimento calculado na classe Roboat
        self.win = win
        self.proj = Roboat(cais,ilhas,impurezas)
        self.largura = abs(cais[0].getP1().getX()-cais[0].getP2().getX())
        self.marker = Circle(Point(cais[0].getCenter().getX() + (self.largura/2)+3,cais[0].getCenter().getY()), 1)
        self.marker.setFill("red")
        self.marker.setOutline("red")
        self.marker.draw(win)
        self.a = self.proj.retornatheta()
        center = self.marker.getCenter()
        self.p1x = center.getX()
        self.p1y = center.getY()
        self.proa = Circle(Point( self.p1x+ cos(self.a), self.p1y + sin(self.a)), 0.5)
        self.proa.setFill('peachpuff')
        self.proa.draw(self.win)

    def update(self,k):
    #representa graficamente o movimento do roboat e da sua proa
        self.proj.update(k)
        self.a = self.proj.retornatheta()
        center = self.marker.getCenter()
        dx = self.proj.getX() - center.getX()
        dy = self.proj.getY() - center.getY()
        self.marker.move(dx, dy)
        self.proa.move(dx, dy)
        self.proa.undraw()
        self.a = self.proj.retornatheta()
        self.proa = Circle(Point(center.getX() + cos(self.a), center.getY() + sin(self.a)), 0.5)
        self.proa.setFill('peachpuff')
        self.proa.draw(self.win)

    def update_retorna_cais(self):
    #representa graficamente o movimento do roboat e da sua proa quando se deslocam em direção ao cais
        self.proj.update_retorna_cais()
        center = self.marker.getCenter()
        dx = self.proj.getX() - center.getX()
        dy = self.proj.getY() - center.getY()
        self.marker.move(dx, dy)
        self.proa.move(dx, dy)
        self.proa.undraw()
        self.a = self.proj.retornatheta()
        self.proa = Circle(Point(center.getX() + cos(self.a), center.getY() + sin(self.a)), 0.5)
        self.proa.setFill('peachpuff')
        self.proa.draw(self.win)

    def update_proa_atracar(self):
    #representa a proa atracada paralelamente ao cais
        self.proa.undraw()
        center = self.marker.getCenter()
        self.proa = Circle(Point(center.getX() + cos(pi/2), center.getY() + sin(pi/2)), 0.5)
        self.proa.setFill('peachpuff')
        self.proa.draw(self.win)

    def getX(self):
    #retorna a coordenada X do roboat
        return self.proj.getX()

    def getY(self):
    # retorna a coordenada X do roboat
        return self.proj.getY()

    def undraw(self):
    #apaga o roboat
        self.marker.undraw()
        self.proa.undraw()

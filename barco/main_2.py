from mapa import *
from animation import *
from time import *
from dados import *
from graphics import *
#José Fernandes, 100209
#Nuno Fernandes, 100242

def main2():
    dados = Dados('Ambiente.txt', 'Impurezas.txt')
    lista_ilhas = dados.retorna_lista_ilhas()
    lista_cais = dados.retorna_lista_cais()
    lista_janela = dados.retorna_lista_janela()
    lista_impurezas = []
    lista_distancia_impureza_temporaria = []
    lista_impurezas_ordenada = []

    map = GraphWin('Mapa', lista_janela[0], lista_janela[1])
    map.setCoords(0, 0, lista_janela[0] / 10, lista_janela[1] / 10)
    update(600)
    map.setBackground(color_rgb(30, 143, 245))

    Cais(map, lista_cais)
    o=Ilha(map, lista_ilhas)

    while len(lista_impurezas) < 5:
        ponto_impureza = map.getMouse()
        if o.limite_obstaculos(lista_ilhas, ponto_impureza) == 'false':
            print('obs')
        else:
            ponto_temporario = Circle(ponto_impureza, 0.5)
            ponto_temporario.setWidth(1)
            ponto_temporario.draw(map)
            lista_impurezas.append(ponto_impureza)
            sleep(0.3)
            ponto_temporario.undraw()

    for i in lista_impurezas:
        distancia = sqrt((i.getX() - lista_cais[0].getCenter().getX()) ** 2 + (
                    i.getY() - lista_cais[0].getCenter().getY()) ** 2)
        lista_distancia_impureza_temporaria.append(tuple([i, distancia]))
    print(lista_distancia_impureza_temporaria)
    lista_distancia_impureza_temporaria.sort(reverse=False, key=lambda x: x[1])
    print(lista_distancia_impureza_temporaria)

    for i in lista_distancia_impureza_temporaria:
        x = list(i)
        lista_impurezas_ordenada.append(x[0])
    print(lista_impurezas_ordenada)

    i = Impurezas(map, lista_impurezas_ordenada)
    largura = abs(lista_cais[0].getP1().getX() - lista_cais[0].getP2().getX())

    roboat = ShotTracker(map, lista_cais, lista_ilhas, lista_impurezas_ordenada)

    for k in range(len(lista_impurezas_ordenada)):
        while abs(roboat.getX() - lista_impurezas_ordenada[k].getX()) > 0.1 or abs(
                roboat.getY() - lista_impurezas_ordenada[k].getY()) > 0.1:
            roboat.update(k)
        else:
            sleep(2)
            i.apaga_impureza(k)
    while abs(roboat.getX() - (lista_cais[0].getCenter().getX() + (largura / 2)+3)) > 0.1 or abs(
            roboat.getY() - lista_cais[0].getCenter().getY()) > 0.1:
        roboat.update_retorna_cais()

    roboat.update_proa_atracar()
    sleep(4)


main2()

from button import *
from mapa import *
from animation import *
from time import *
from dados import *
from random import *


# José Fernandes, 100209
# Nuno Fernandes, 100242

def criar_impurezas(map, lista_impurezas, lista_ilhas, o, j):
    # cria as impurezas com base em cliques do utilizador
    while len(lista_impurezas) < j:
        ponto_impureza = map.getMouse()
        if o.limite_obstaculos(lista_ilhas, ponto_impureza) == 'false':
            print('obs')
        else:
            ponto_temporario = Circle(ponto_impureza, 0.5)
            ponto_temporario.setWidth(1)
            ponto_temporario.draw(map)
            lista_impurezas.append(ponto_impureza)
            sleep(0.3)
            ponto_temporario.undraw()


def ordenar_lista_impurezas(lista_cais, lista_distancia_impureza_temporaria, lista_impurezas, lista_impurezas_ordenada):
    # ordena as impurezas com base na sua distancia ao cais
    for i in lista_impurezas:
        distancia = sqrt((i.getX() - lista_cais[0].getCenter().getX()) ** 2 + (
                i.getY() - lista_cais[0].getCenter().getY()) ** 2)
        lista_distancia_impureza_temporaria.append(tuple([i, distancia]))
    lista_distancia_impureza_temporaria.sort(reverse=False, key=lambda x: x[1])
    for i in lista_distancia_impureza_temporaria:
        x = list(i)
        lista_impurezas_ordenada.append(x[0])


def impurezas_aleatorias(lista_ilhas, lista_impurezas, lista_impurezas_possiveis, o):
    # através de uma lista é selecionado um numero aleatório ,de 4 a 7 , de impurezas e avalia se coincidem com obstáculos
    lista_importada_impurezas = open('Impurezas_aleatorias.txt', 'r').readlines()
    for n in lista_importada_impurezas:
        if o.limite_obstaculos(lista_ilhas, eval(n)) == 'true':
            lista_impurezas_possiveis.append(eval(n))
    for i in range(round(randrange(4, 8))):
        n = round(randrange(0, len(lista_impurezas_possiveis)))
        lista_impurezas.append(lista_impurezas_possiveis[n])
        del (lista_impurezas_possiveis[n])


def ilhas_aleatorias(lista_ilhas, lista_ilhas_possiveis):
    # através de uma lista são selecionadas um numero aleatório de ilhas
    for i in range(round(randrange(2, 5))):
        n = round(randrange(0, len(lista_ilhas_possiveis)))
        lista_ilhas.append(lista_ilhas_possiveis[n])
        del (lista_ilhas_possiveis[n])


def main():
    # função correspondente à primeira implementação
    dados = Dados('Ambiente.txt', 'Impurezas.txt')
    lista_ilhas = dados.retorna_lista_ilhas()
    lista_cais = dados.retorna_lista_cais()
    lista_janela = dados.retorna_lista_janela()
    lista_impurezas = []
    map = GraphWin('Mapa', lista_janela[0], lista_janela[1])
    map.setCoords(0, 0, lista_janela[0] / 10, lista_janela[1] / 10)
    update(200)
    map.setBackground(color_rgb(30, 143, 245))
    roboat = ShotTracker(map, lista_cais, lista_ilhas, lista_impurezas)
    Cais(map, lista_cais)
    o = Ilha(map, lista_ilhas)
    criar_impurezas(map, lista_impurezas, lista_ilhas, o, 1)
    i = Impurezas(map, lista_impurezas)
    roboat.undraw()
    largura = abs(lista_cais[0].getP1().getX() - lista_cais[0].getP2().getX())
    roboat = ShotTracker(map, lista_cais, lista_ilhas, lista_impurezas)
    condicoes_movimento(i, largura, lista_cais, lista_impurezas, roboat)
    roboat.update_proa_atracar()
    sleep(2)
    map.close()


def condicoes_movimento(i, largura, lista_cais, lista_impurezas, roboat):
    # avalia e executa as várias fases do movimento na implementação 1,
    # com base na posição do roboat e dos objetos, incluindo a remoção das impurezas
    for k in range(len(lista_impurezas)):
        while abs(roboat.getX() - lista_impurezas[k].getX()) > 0.1 or abs(
                roboat.getY() - lista_impurezas[k].getY()) > 0.1:
            roboat.update(k)
        else:
            sleep(2)
            i.apaga_impureza(k)
    while abs(roboat.getX() - (lista_cais[0].getCenter().getX() + (largura / 2) + 3)) > 0.1 or abs(
            roboat.getY() - lista_cais[0].getCenter().getY()) > 0.1:
        roboat.update_retorna_cais()


def main2():
    # função correspondente à segunda implementação
    dados = Dados('Ambiente.txt', 'Impurezas.txt')
    lista_ilhas = dados.retorna_lista_ilhas()
    lista_cais = dados.retorna_lista_cais()
    lista_janela = dados.retorna_lista_janela()
    lista_impurezas = []
    lista_distancia_impureza_temporaria = []
    lista_impurezas_ordenada = []
    map = GraphWin('Mapa', lista_janela[0], lista_janela[1])
    update(200)
    map.setCoords(0, 0, lista_janela[0] / 10, lista_janela[1] / 10)
    map.setBackground(color_rgb(30, 143, 245))
    roboat = ShotTracker(map, lista_cais, lista_ilhas, lista_impurezas)
    Cais(map, lista_cais)
    o = Ilha(map, lista_ilhas)
    criar_impurezas(map, lista_impurezas, lista_ilhas, o, 5)
    ordenar_lista_impurezas(lista_cais, lista_distancia_impureza_temporaria, lista_impurezas, lista_impurezas_ordenada)
    i = Impurezas(map, lista_impurezas_ordenada)
    roboat.undraw()
    largura = abs(lista_cais[0].getP1().getX() - lista_cais[0].getP2().getX())
    roboat = ShotTracker(map, lista_cais, lista_ilhas, lista_impurezas_ordenada)
    condicoes_movimento_2(i, largura, lista_cais, lista_impurezas_ordenada, roboat)
    roboat.update_proa_atracar()
    sleep(2)
    map.close()


def condicoes_movimento_2(i, largura, lista_cais, lista_impurezas_ordenada, roboat):
    # avalia e executa as várias fases do movimento na implementação 2,
    # com base na posição do roboat e dos objetos, incluindo a remoção das impurezas
    for k in range(len(lista_impurezas_ordenada)):
        while abs(roboat.getX() - lista_impurezas_ordenada[k].getX()) > 0.1 or abs(
                roboat.getY() - lista_impurezas_ordenada[k].getY()) > 0.1:
            roboat.update(k)
        else:
            sleep(2)
            i.apaga_impureza(k)
    while abs(roboat.getX() - (lista_cais[0].getCenter().getX() + (largura / 2) + 3)) > 0.1 or abs(
            roboat.getY() - lista_cais[0].getCenter().getY()) > 0.1:
        roboat.update_retorna_cais()


def main3():
    # função correspondente à terceira implementação, com obstáculo e impurezas lidas de um ficheiro
    dados = Dados('Ambiente.txt', 'Impurezas.txt')
    lista_ilhas = dados.retorna_lista_ilhas()
    lista_impurezas = dados.retorna_lista_impurezas()
    lista_cais = dados.retorna_lista_cais()
    lista_janela = dados.retorna_lista_janela()
    map = GraphWin('Mapa', lista_janela[0], lista_janela[1])
    map.setCoords(0, 0, lista_janela[0] / 10, lista_janela[1] / 10)
    update(200)
    map.setBackground(color_rgb(30, 143, 245))
    roboat = ShotTracker(map, lista_cais, lista_ilhas, lista_impurezas)
    Cais(map, lista_cais)
    Ilha(map, lista_ilhas)
    i = Impurezas(map, lista_impurezas)
    roboat.undraw()
    largura = abs(lista_cais[0].getP1().getX() - lista_cais[0].getP2().getX())
    roboat = ShotTracker(map, lista_cais, lista_ilhas, lista_impurezas)
    condicoes_movimento_3(i, largura, lista_cais, lista_impurezas, roboat)
    roboat.update_proa_atracar()
    sleep(2)
    map.close()


def condicoes_movimento_3(i, largura, lista_cais, lista_impurezas, roboat):
    # avalia e executa as várias fases do movimento na implementação 3,
    # com base na posição do roboat e dos objetos, incluindo a remoção das impurezas
    for k in range(len(lista_impurezas)):
        while abs(roboat.getX() - lista_impurezas[k].getX()) > 0.1 or abs(
                roboat.getY() - lista_impurezas[k].getY()) > 0.1:
            roboat.update(k)
        else:
            sleep(2)
            i.apaga_impureza(k)
    while abs(roboat.getX() - (lista_cais[0].getCenter().getX() + (largura / 2) + 3)) > 0.1 or abs(
            roboat.getY() - lista_cais[0].getCenter().getY()) > 0.1:
        roboat.update_retorna_cais()


def main3b():
    # função correspondente à terceira implementação, com obstáculos aleatórios e impurezas clicadas
    lista_cais = [Rectangle(Point(0, 0), Point(5, 5))]
    lista_janela = [800, 600]
    lista_impurezas = []
    lista_distancia_impureza_temporaria = []
    lista_impurezas_ordenada = []
    lista_ilhas = []
    lista_ilhas_possiveis = [Circle(Point(50, 50), 5), Circle(Point(20, 30), 3), Circle(Point(10, 10), 3),
                             Circle(Point(65, 45), 5), Circle(Point(70, 25), 4), Circle(Point(8, 40), 3),
                             Circle(Point(12, 50), 4), Circle(Point(50, 12), 3), Circle(Point(40, 30), 5),
                             Circle(Point(5, 20), 2)]
    ilhas_aleatorias(lista_ilhas, lista_ilhas_possiveis)
    map = GraphWin('Mapa', lista_janela[0], lista_janela[1])
    map.setCoords(0, 0, lista_janela[0] / 10, lista_janela[1] / 10)
    update(200)
    map.setBackground(color_rgb(30, 143, 245))
    roboat = ShotTracker(map, lista_cais, lista_ilhas, lista_impurezas)
    Cais(map, lista_cais)
    o = Ilha(map, lista_ilhas)
    criar_impurezas(map, lista_impurezas, lista_ilhas, o, 5)
    ordenar_lista_impurezas(lista_cais, lista_distancia_impureza_temporaria, lista_impurezas, lista_impurezas_ordenada)
    i = Impurezas(map, lista_impurezas_ordenada)
    roboat.undraw()
    largura = abs(lista_cais[0].getP1().getX() - lista_cais[0].getP2().getX())
    roboat = ShotTracker(map, lista_cais, lista_ilhas, lista_impurezas_ordenada)
    condicoes_movimento_3b(i, largura, lista_cais, lista_impurezas_ordenada, roboat)
    roboat.update_proa_atracar()
    sleep(2)
    map.close()


def condicoes_movimento_3b(i, largura, lista_cais, lista_impurezas_ordenada, roboat):
    # avalia e executa as várias fases do movimento na implementação 3,
    # com base na posição do roboat e dos objetos, incluindo a remoção das impurezas
    for k in range(len(lista_impurezas_ordenada)):
        while abs(roboat.getX() - lista_impurezas_ordenada[k].getX()) > 0.1 or abs(
                roboat.getY() - lista_impurezas_ordenada[k].getY()) > 0.1:
            roboat.update(k)
        else:
            sleep(2)
            i.apaga_impureza(k)
    while abs(roboat.getX() - (lista_cais[0].getCenter().getX() + (largura / 2) + 3)) > 0.1 or abs(
            roboat.getY() - lista_cais[0].getCenter().getY()) > 0.1:
        roboat.update_retorna_cais()


def main3c():
    # função correspondente à terceira implementação, com obstáculos aleatórios e impurezas aleatórias
    lista_cais = [Rectangle(Point(0, 0), Point(5, 5))]
    lista_janela = [800, 600]
    lista_impurezas = []
    lista_distancia_impureza_temporaria = []
    lista_impurezas_ordenada = []
    lista_impurezas_possiveis = []
    lista_ilhas = []
    lista_ilhas_possiveis = [Circle(Point(50, 50), 5), Circle(Point(20, 30), 3), Circle(Point(10, 10), 3),
                             Circle(Point(65, 45), 5), Circle(Point(70, 25), 4), Circle(Point(8, 40), 3),
                             Circle(Point(12, 50), 4), Circle(Point(50, 12), 3), Circle(Point(40, 30), 5),
                             Circle(Point(5, 20), 2)]
    ilhas_aleatorias(lista_ilhas, lista_ilhas_possiveis)
    map = GraphWin('Mapa', lista_janela[0], lista_janela[1])
    map.setCoords(0, 0, lista_janela[0] / 10, lista_janela[1] / 10)
    update(200)
    map.setBackground(color_rgb(30, 143, 245))
    roboat = ShotTracker(map, lista_cais, lista_ilhas, lista_impurezas)
    Cais(map, lista_cais)
    o = Ilha(map, lista_ilhas)
    impurezas_aleatorias(lista_ilhas, lista_impurezas, lista_impurezas_possiveis, o)
    ordenar_lista_impurezas(lista_cais, lista_distancia_impureza_temporaria, lista_impurezas, lista_impurezas_ordenada)
    i = Impurezas(map, lista_impurezas_ordenada)
    roboat.undraw()
    largura = abs(lista_cais[0].getP1().getX() - lista_cais[0].getP2().getX())
    roboat = ShotTracker(map, lista_cais, lista_ilhas, lista_impurezas_ordenada)
    condicoes_movimento_3c(i, largura, lista_cais, lista_impurezas_ordenada, roboat)
    roboat.update_proa_atracar()
    sleep(2)
    map.close()


def condicoes_movimento_3c(i, largura, lista_cais, lista_impurezas_ordenada, roboat):
    # avalia e executa as várias fases do movimento na implementação 3,
    # com base na posição do roboat e dos objetos, incluindo a remoção das impurezas
    for k in range(len(lista_impurezas_ordenada)):
        while abs(roboat.getX() - lista_impurezas_ordenada[k].getX()) > 0.1 or abs(
                roboat.getY() - lista_impurezas_ordenada[k].getY()) > 0.1:
            roboat.update(k)
        else:
            sleep(2)
            i.apaga_impureza(k)
    while abs(roboat.getX() - (lista_cais[0].getCenter().getX() + (largura / 2) + 3)) > 0.1 or abs(
            roboat.getY() - lista_cais[0].getCenter().getY()) > 0.1:
        roboat.update_retorna_cais()


def main_geral():
    # cria o menu gráfico que permite ao utilizador escolher a implementação
    GUI_principal = GraphWin('GUI', 700, 500)
    GUI_principal.setCoords(0, 0, 50, 50)
    menssagem = Text(Point(25, 35), "Selecione a implementação:")
    menssagem.draw(GUI_principal)
    botao_1_p = Button(GUI_principal, Point(10, 20), 15, 8, 'Implementação 1')
    botao_2_p = Button(GUI_principal, Point(25, 20), 15, 8, 'Implementação 2')
    botao_3_p = Button(GUI_principal, Point(40, 20), 15, 8, 'Implementação 3')
    click_janela = GUI_principal.getMouse()
    condicoes_botoes(botao_1_p, botao_2_p, botao_3_p, click_janela, GUI_principal)


def condicoes_botoes(botao_1_p, botao_2_p, botao_3_p, click_janela, GUI_principal):
    # executa a implementação selecionada, com base no botão clicado
    if botao_1_p.clicked(click_janela) == 'true':
        main()
        GUI_principal.close()
        main_geral()
    if botao_2_p.clicked(click_janela) == 'true':
        main2()
        GUI_principal.close()
        main_geral()
    if botao_3_p.clicked(click_janela) == 'true':
        GUI3 = GraphWin('GUI', 700, 500)
        GUI3.setCoords(0, 0, 50, 50)
        botao_1 = Button(GUI3, Point(12.55, 37.55), 23, 4, 'Obstáculos de ficheiro / Impurezas clicadas')
        botao_2 = Button(GUI3, Point(12.55, 12.55), 23, 4, 'Obstáculos de ficheiro / Impurezas de ficheiro')
        botao_3 = Button(GUI3, Point(37.55, 37.55), 23, 4, 'Obstáculos aleatórios / Impurezas clicadas')
        botao_4 = Button(GUI3, Point(37.55, 12.55), 23, 4, 'Obstáculos aleatórios / Impurezas aleatórias ')
        click_janela_3 = GUI3.getMouse()
        condicoes_botoes_2(GUI3, GUI_principal, botao_1, botao_2, botao_3, botao_4, click_janela_3)


def condicoes_botoes_2(GUI3, GUI_principal, botao_1, botao_2, botao_3, botao_4, click_janela_3):
    # executa a variação da implementação 3 selecionada, com base no botão clicado
    if botao_1.clicked(click_janela_3):
        main2()
        GUI3.close()
        GUI_principal.close()
        main_geral()
    if botao_2.clicked(click_janela_3):
        main3()
        GUI3.close()
        GUI_principal.close()
        main_geral()
    if botao_3.clicked(click_janela_3):
        main3b()
        GUI3.close()
        GUI_principal.close()
        main_geral()
    if botao_4.clicked(click_janela_3):
        main3c()
        GUI3.close()
        GUI_principal.close()
        main_geral()


main_geral()

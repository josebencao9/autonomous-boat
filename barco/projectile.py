from math import *
#José Fernandes, 100209
#Nuno Fernandes, 100242

class Roboat:

    def __init__(self, cais, ilhas, impurezas):
    #recebe da função main listas e calcula as posições do roboat
        self.lista_cais = cais
        self.lista_ilhas = ilhas
        self.lista_impurezas = impurezas
        self.largura = abs(self.lista_cais[0].getP1().getX() - self.lista_cais[0].getP2().getX())
        self.roboat_pos = [(self.lista_cais[0].getCenter()).getX() + (self.largura / 2) + 3,
                           (self.lista_cais[0].getCenter()).getY()]
        self.theta = pi/2

    def update(self, k):
    #movimenta o roboat até às impurezas, tendo em atenção os obstáculos e a melhor forma de contorná-los
        self.theta = atan(float(self.lista_impurezas[k].getY() - self.roboat_pos[1]) /
                          float(self.lista_impurezas[k].getX() - self.roboat_pos[0]))
        if float(self.lista_impurezas[k].getX()) < float(self.roboat_pos[0]):
            self.theta = self.theta + pi
        self.movimento()
        for n in range(len(self.lista_ilhas)):
            if (float(self.lista_ilhas[n].getCenter().getX()) - self.roboat_pos[0]) ** 2 + (
                    float(self.lista_ilhas[n].getCenter().getY()) - self.roboat_pos[1]) ** 2 < (
                    float(self.lista_ilhas[n].getRadius()) + 1.5) ** 2:
                m = ((float(self.lista_impurezas[k].getY()) - float(self.roboat_pos[1])) / (
                        float(self.lista_impurezas[k].getX()) - float(self.roboat_pos[0])))
                b = float(self.lista_impurezas[k].getY()) - m * float(self.lista_impurezas[k].getX())
                y = m * float(self.lista_ilhas[n].getCenter().getX()) + b
                self.esquerda_ou_direita(k, n, y)
                self.movimento()
            elif (float(0) - self.roboat_pos[0]) ** 2 + (
                    float(0) - self.roboat_pos[1]) ** 2 < (
                    float(sqrt(50)) + 1) ** 2:
                if float(self.lista_impurezas[k].getY()) > float(self.roboat_pos[1]):
                    self.theta = self.theta - (pi / 2)
                else:
                    self.theta = self.theta + (pi / 2)
                self.movimento()

    def esquerda_ou_direita(self, k, n, y):
    #determina a melhor forma de contornar os obstáculos, quando o roboat se dirige para as impurezas
        if y > float(self.lista_ilhas[n].getCenter().getY()) and float(self.lista_impurezas[k].getY()) > float(
                self.roboat_pos[1]) and float(self.lista_impurezas[k].getX()) > float(self.roboat_pos[0]):
            self.theta = self.theta + (pi / 2)
        elif y > float(self.lista_ilhas[n].getCenter().getY()) and float(
                self.lista_impurezas[k].getY()) < float(
            self.roboat_pos[1]) and float(self.lista_impurezas[k].getX()) > float(self.roboat_pos[0]):
            self.theta = self.theta + (pi / 2)
        elif y < float(self.lista_ilhas[n].getCenter().getY()) and float(
                self.lista_impurezas[k].getY()) < float(
            self.roboat_pos[1]) and float(self.lista_impurezas[k].getX()) < float(self.roboat_pos[0]):
            self.theta = self.theta + (pi / 2)
        elif y < float(self.lista_ilhas[n].getCenter().getY()) and float(
                self.lista_impurezas[k].getY()) > float(
            self.roboat_pos[1]) and float(self.lista_impurezas[k].getX()) < float(self.roboat_pos[0]):
            self.theta = self.theta + (pi / 2)
        else:
            self.theta = self.theta - (pi / 2)

    def update_retorna_cais(self):
    #movimenta o roboat até ao cais, tendo em atenção os obstáculos e a melhor forma de contorná-los
        self.theta = atan(float(self.lista_cais[0].getCenter().getY() - self.roboat_pos[1]) /
                          float(
                              (self.lista_cais[0].getCenter().getX() + (self.largura / 2) + 3) - (self.roboat_pos[0])))
        if float((self.lista_cais[0].getCenter()).getX() + (self.largura / 2) + 3) < float(self.roboat_pos[0]):
            self.theta = self.theta + pi
        self.movimento()
        for n in range(len(self.lista_ilhas)):
            if (float(self.lista_ilhas[n].getCenter().getX()) - self.roboat_pos[0]) ** 2 + (
                    float(self.lista_ilhas[n].getCenter().getY()) - self.roboat_pos[1]) ** 2 < (
                    float(self.lista_ilhas[n].getRadius()) + 1.5) ** 2:
                m = ((float(self.lista_cais[0].getCenter().getY()) - float(self.roboat_pos[1])) / (
                        float(self.lista_cais[0].getCenter().getX() + (self.largura / 2) + 3) - float(
                    self.roboat_pos[0])))
                b = float(self.lista_cais[0].getCenter().getY()) - m * float(
                    self.lista_cais[0].getCenter().getX() + (self.largura / 2) + 3)
                y = m * float(self.lista_ilhas[n].getCenter().getX()) + b
                self.esquerda_ou_direita_retorna_cais(n, y)
                self.movimento()
            elif (float(0) - self.roboat_pos[0]) ** 2 + (
                    float(0) - self.roboat_pos[1]) ** 2 < (
                    float(sqrt(50)) + 1) ** 2:
                if float((self.lista_cais[0].getCenter()).getY()) > float(self.roboat_pos[1]):
                    self.theta = self.theta - (pi / 2)
                else:
                    self.theta = self.theta + (pi / 2)
                self.movimento()

    def esquerda_ou_direita_retorna_cais(self, n, y):
    #determina a melhor forma de contornar os obstáculos, quando o roboat se dirige para o cais
        if y > float(self.lista_ilhas[n].getCenter().getY()) and float(
                self.lista_cais[0].getCenter().getY()) > float(
            self.roboat_pos[1]) and float(
            self.lista_cais[0].getCenter().getX() + (self.largura / 2) + 3) > float(self.roboat_pos[0]):
            self.theta = self.theta + (pi / 2)
        elif y > float(self.lista_ilhas[n].getCenter().getY()) and float(
                self.lista_cais[0].getCenter().getY()) < float(
            self.roboat_pos[1]) and float(
            self.lista_cais[0].getCenter().getX() + (self.largura / 2) + 3) > float(self.roboat_pos[0]):
            self.theta = self.theta + (pi / 2)
        elif y < float(self.lista_ilhas[n].getCenter().getY()) and float(
                self.lista_cais[0].getCenter().getY()) < float(
            self.roboat_pos[1]) and float(
            self.lista_cais[0].getCenter().getX() + (self.largura / 2) + 3) < float(self.roboat_pos[0]):
            self.theta = self.theta + (pi / 2)
        elif y < float(self.lista_ilhas[n].getCenter().getY()) and float(
                self.lista_cais[0].getCenter().getY()) > float(
            self.roboat_pos[1]) and float(
            self.lista_cais[0].getCenter().getX() + (self.largura / 2) + 3) < float(self.roboat_pos[0]):
            self.theta = self.theta + (pi / 2)
        else:
            self.theta = self.theta - (pi / 2)

    def movimento(self):
    #atualiza a posição do roboat durante o seu movimento
        self.xvel = 10 * cos(self.theta)
        self.yvel = 10 * sin(self.theta)
        self.roboat_pos[0] = self.roboat_pos[0] + 0.002 * self.xvel
        self.roboat_pos[1] = self.roboat_pos[1] + 0.002 * self.yvel

    def getY(self):
    #retorna a coordenada Y atual do roboat
        return self.roboat_pos[1]

    def getX(self):
    #retorna a coordenada X atual do roboat
        return self.roboat_pos[0]

    def retornatheta(self):
    #retorna o valor do ângulo do vetor velocidade do roboat
        return self.theta
